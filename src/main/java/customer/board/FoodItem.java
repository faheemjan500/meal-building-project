/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package customer.board;

public interface FoodItem {

  String name();

  Packing packing();

  float price();
}
