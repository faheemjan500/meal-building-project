/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package builder.example.meal.building.creation;

public class MealBuilder {

  public Meal NonVegenMeal() {
    final Meal meal = new Meal();
    meal.addFoodItem(new ChickenBurger());
    meal.addFoodItem(new Coke());
    return meal;
  }

  public Meal prepareVeganMeal() {

    final Meal meal = new Meal();
    meal.addFoodItem(new VegBurger());
    meal.addFoodItem(new Pepsi());
    return meal;
  }

}
