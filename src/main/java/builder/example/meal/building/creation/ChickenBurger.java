/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

public class ChickenBurger extends Burger {

  public String name() {
    return "Chicken-Burger";
  }

  @Override
  public float price() {
    return 50.5f;
  }

}
