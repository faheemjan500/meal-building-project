/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

import customer.board.Packing;

public class Wrapper implements Packing {

  public String pack() {
    return "wrapper";
  }

}
