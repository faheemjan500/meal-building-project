/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

import customer.board.FoodItem;
import customer.board.Packing;

public abstract class ColdDrink implements FoodItem {
  public Packing packing() {
    return new Bottle();
  }

  public abstract float price();

}
