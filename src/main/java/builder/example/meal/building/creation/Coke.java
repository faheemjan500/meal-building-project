/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package builder.example.meal.building.creation;

public class Coke extends ColdDrink {

  public String name() {
    return "Coke";
  }

  @Override
  public float price() {
    return 1.20f;
  }

}
