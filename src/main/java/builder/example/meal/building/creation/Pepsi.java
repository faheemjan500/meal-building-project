/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package builder.example.meal.building.creation;

public class Pepsi extends ColdDrink {

  public String name() {
    return "pepsi";
  }

  @Override
  public float price() {
    return 1.0f;
  }

}
