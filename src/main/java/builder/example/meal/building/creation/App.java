/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

/**
 * Hello world!
 *
 */
public class App {
  public static void main(String[] args) {
    final MealBuilder mealBuilder = new MealBuilder();
    final Meal meal = mealBuilder.prepareVeganMeal();
    meal.showItems();
    System.out.println("Total price of the MEAL is : " + meal.getCost());

  }
}
