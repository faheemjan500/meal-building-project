/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

import java.util.ArrayList;
import java.util.List;

import customer.board.FoodItem;

public class Meal {

  private final List<FoodItem> items = new ArrayList<FoodItem>();

  public void addFoodItem(FoodItem item) {
    items.add(item);
  }

  public float getCost() {
    float cost = 0;
    for (final FoodItem item : items) {
      cost = cost + item.price();
    }
    return cost;
  }

  public void showItems() {

    for (final FoodItem item : items) {
      System.out.println("Food item name :" + item.name() + "\n");
      System.out.println("Packing of the food item is :" + item.packing().pack() + "\n");
      System.out.println("Price of the Food item is : " + item.price() + "\n");
    }
  }

}
