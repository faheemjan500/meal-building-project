/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package builder.example.meal.building.creation;

import customer.board.FoodItem;
import customer.board.Packing;

abstract class Burger implements FoodItem {
  public Packing packing() {
    return new Wrapper();
  }

  public abstract float price();
}
