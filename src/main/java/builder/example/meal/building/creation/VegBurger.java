/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package builder.example.meal.building.creation;

public class VegBurger extends Burger {

  public String name() {

    return "Vegetable-Burger";
  }

  @Override
  public float price() {

    return 25.0f;
  }

}
